# goJavaApi

#### 介绍
jdk里内置的java源码，通常我们需要查看其api文档，可以到oracle官网查询，该工具通过idea插件的方式快速跳转到当前类在oracle java api文档的网页端



#### 安装教程
方式1：goJavaApi.jar 本地安装
方式2：idea插件在线搜索 goJavaApi 安装

#### 使用说明

1.  找到jdk内相应的java类
2.  点击java文件内任意位置
3.  右键 -> search java api

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 联系方式
1.  QQ:834033206


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
