package base;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.yourkit.util.Strings;
import util.PsiClassUtil;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 * 上下文信息
 * @Description: <br>
 * @Project: hades <br>
 * @CreateDate: Created in 2018/8/22 09:52 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class ActionContext {
    private String rootPath;//根路径

    private String domainPath;//当前实体类路径

    private String className;//类名

    private String projectName;//项目名称

    private PsiClass psiClass;//class信息

    private Project project;//项目

    private AnActionEvent event;//事件


    public static ActionContext valueOf(AnActionEvent event, PsiClass psiClass) {
        ActionContext context = new ActionContext();

        context.setEvent(event);
        context.setPsiClass(psiClass);

        Project project = event.getProject();
        if (project == null) {
            throw new RuntimeException("project is null");
        }

        context.setProject(project);

        String rootPath = project.getBasePath();
        if (Strings.isNullOrEmpty(rootPath)) {
            throw new RuntimeException("basePath is null");
        }
        context.setRootPath(rootPath);

        context.setDomainPath(PsiClassUtil.getSourcePath(psiClass));

        context.setClassName(psiClass.getName());
        
        context.setProjectName(rootPath.substring(rootPath.lastIndexOf("/") + 1));

        return context;
    }


    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getDomainPath() {
        return domainPath;
    }

    public void setDomainPath(String domainPath) {
        this.domainPath = domainPath;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public PsiClass getPsiClass() {
        return psiClass;
    }

    public void setPsiClass(PsiClass psiClass) {
        this.psiClass = psiClass;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public AnActionEvent getEvent() {
        return event;
    }

    public void setEvent(AnActionEvent event) {
        this.event = event;
    }
}
